package decorator;

public abstract class Imposto {

	protected final Imposto outroImposto;

	public Imposto() {
		outroImposto = null;
	}
	
	public Imposto(Imposto outroImposto) {
		this.outroImposto = outroImposto;
	}

	abstract double calcular(Orcamento orcamento);

	protected double calculoDoOutroImposto(Orcamento orcamento) {
		// tratando o caso do proximo imposto nao existir!
        if(outroImposto == null) return 0;
        return outroImposto.calcular(orcamento);
	}
}
