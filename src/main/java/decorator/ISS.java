package decorator;

public class ISS extends Imposto {

	public ISS() {

	}

	public ISS(Imposto outroImposto) {
		super(outroImposto);
	}

	public double calcular(Orcamento orcamento) {
		return orcamento.getValor() * 0.06 + calculoDoOutroImposto(orcamento);
	}

}
