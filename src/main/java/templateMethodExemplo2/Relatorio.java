package templateMethodExemplo2;

import java.util.List;

public abstract class Relatorio {
	
	protected abstract void cabecalho();
    protected abstract void rodape();
    protected abstract void corpo(List<Conta> contas);

   final public void imprime(List<Conta> contas) {
      cabecalho();
      corpo(contas);
      rodape();
    }

}
