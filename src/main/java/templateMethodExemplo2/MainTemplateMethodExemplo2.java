package templateMethodExemplo2;

import java.util.ArrayList;
import java.util.List;

public class MainTemplateMethodExemplo2 {
	
	public static void main(String[] args) {
		Relatorio relatorioSimples = new RelatorioSimples();
		Relatorio relatorioComplexo = new RelatorioComplexo();
		
		List<Conta> contas = new ArrayList<Conta>();
		contas.add(new Conta("conta 1", "100-2", "1000-4", 1000.0));
		contas.add(new Conta("conta 2", "150-2", "1500-4", 5000.0));
		relatorioSimples.imprime(contas);
		System.out.println("\n\n");
		relatorioComplexo.imprime(contas);
	}

}
