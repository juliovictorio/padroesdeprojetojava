package stateExemplo2;

public class Conta {

	protected int saldo;
	protected EstadoDaConta estado;

	public Conta() {
		estado = new Positivo();
	}

	public void saca(double valor) {
		estado.saca(this, valor);
	}

	public void deposita(double valor) {
		estado.deposita(this, valor);
	}

}
