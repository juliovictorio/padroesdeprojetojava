package templateMethod;

public class MainTemplateMethod {

	public static void main(String[] args) {
		ICPP icpp = new ICPP();
		IKCV ikcv = new IKCV();
		
		Orcamento orcamentoMaiorQue500 = new Orcamento(600);
		double result1 = icpp.calcular(orcamentoMaiorQue500);
		System.out.println(result1);
		
		Orcamento orcamentoMaiorQue500EItemMaiorQue100 = new Orcamento(600);
		orcamentoMaiorQue500EItemMaiorQue100.adicionaItem(new Item("CANETA IMPORTADA", 120.0));
		double result2 = ikcv.calcular(orcamentoMaiorQue500EItemMaiorQue100);
		System.out.println(result2);
	}
}
