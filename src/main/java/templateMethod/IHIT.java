package templateMethod;

import java.util.ArrayList;
import java.util.List;

public class IHIT extends TemplateDeImpostoCondicional {

	@Override
	protected boolean deveUsarMaximaTaxacao(Orcamento orcamento) {
		if (contemDoisItensComMesmoNome(orcamento)) {
			return true;
		}else{
			return false;
		}
	}

	private boolean contemDoisItensComMesmoNome(Orcamento orcamento) {
		List<String> noOrcamento = new ArrayList<String>();

        for(Item item : orcamento.getItens()) {
          if(noOrcamento.contains(item.getNome())) return true;
          else noOrcamento.add(item.getNome());
        }

        return false;
	}

	@Override
	protected double maximaTaxacao(Orcamento orcamento) {
		return ((orcamento.getValor() * 0.13)+100.0);
	}

	@Override
	protected double minimaTaxacao(Orcamento orcamento) {
		return orcamento.getValor() * (0.01 * orcamento.getItens().size());
	}

}
