package templateMethod;

public interface Imposto {

	double calcular(Orcamento orcamento);

}
