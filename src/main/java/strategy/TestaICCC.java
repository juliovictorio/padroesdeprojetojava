package strategy;

public class TestaICCC {
	public static void main(String[] args) {
        Orcamento reforma = new Orcamento(500.0);

        Imposto novoImposto = new ICCC();
        System.out.println(novoImposto.calcular(reforma));
      }
}
