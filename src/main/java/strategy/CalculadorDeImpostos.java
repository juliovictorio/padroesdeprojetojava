package strategy;

public class CalculadorDeImpostos {

	public void realizaCalculo(Orcamento orcamento, Imposto imposto) {

		double valor = imposto.calcular(orcamento);

		System.out.println(valor);

	}
}
