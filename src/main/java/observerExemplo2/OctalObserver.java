package observerExemplo2;

public class OctalObserver extends Observer {
	public OctalObserver(Pessoa pessoa) {
		this.pessoa = pessoa;
		this.pessoa.registrarObservers(this);
	}

	public void atualizar() {
		System.out.println("Idade em Octal: " +
				Integer.toOctalString(pessoa.getIdade()));
	}

}
