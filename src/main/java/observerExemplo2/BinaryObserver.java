package observerExemplo2;

public class BinaryObserver extends Observer {

	public BinaryObserver(Pessoa pessoa) {
		this.pessoa = pessoa;
		this.pessoa.registrarObservers(this);
	}

	public void atualizar() {
		System.out.println("Idade em Bin�rio: " + Integer.toBinaryString(pessoa.getIdade()));
	}

}
