package observerExemplo2;

public abstract class Observer {

	protected Pessoa pessoa;

	public abstract void atualizar();

}
