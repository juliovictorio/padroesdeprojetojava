package observerExemplo2;

import java.util.ArrayList;
import java.util.List;

public class Pessoa {

	private List<Observer> listaObservers = new ArrayList<Observer>();
	private int idade;

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
		notificarTodosObservers();
	}

	public void registrarObservers(Observer observer) {
		listaObservers.add(observer);
	}

	public void notificarTodosObservers() {
		for (Observer observer : listaObservers) {
			observer.atualizar();
		}
	}
}
