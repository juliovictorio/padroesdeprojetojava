package observerExemplo2;

public class HexaObserver extends Observer {

	public HexaObserver(Pessoa pessoa) {
		this.pessoa = pessoa;
		this.pessoa.registrarObservers(this);
	}

	public void atualizar() {
		System.out.println("Idade em Hexadecimal: " +
				Integer.toHexString(pessoa.getIdade()).toUpperCase());
	}

}
