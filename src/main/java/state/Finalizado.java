package state;

public class Finalizado implements EstadoDeUmOrcamento{

	public void aplicaDescontoExtra(Orcamento orcamento) {
        throw new RuntimeException("Or�amentos finalizados n�o recebem desconto extra!");
      }

	public void aprova(Orcamento orcamento) {
		throw new RuntimeException("Or�amento j� finalizado");
		
	}

	public void reprova(Orcamento orcamento) {
		throw new RuntimeException("Or�amento j� finalizado");
		
	}

	public void finaliza(Orcamento orcamento) {
		throw new RuntimeException("Or�amento j� finalizado");
		
	}
}
