package state;

public interface Imposto {

	double calcular(Orcamento orcamento);

}
